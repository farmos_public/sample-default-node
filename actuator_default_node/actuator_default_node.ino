/*

  @file sample_default_node.ino
  @data 2020-07-20

  Copyright (c) 2020 JiNong, Inc.
  All right reserved.

*/

#include <Modbus.h>
#include <ModbusSerial.h>
#include <SoftwareSerial.h>

/*
  BAUD = 9600, Slave ID = 2, TXPIN =7
*/
#define BAUD 9600
#define ID 2
#define TXPIN 7

/*
  노드정보 크기 8
  연결제품코드 크기 24
  노드 상태 크기 2
  개폐기 개수 8
  구동기에서 읽어오는 주소 203
  구동기로 보내는 주소 503
*/
#define NODEINFO 8
#define CONNECTEDPRODURCT 24
#define NODESTATUS 2
#define COUNTRETRACT 8
#define READACTUATORREG 203
#define WRITEACTUATORREG 503

/*
  릴레이 1,2,3,4 핀 각각 설정
  relay[0],[1] : 스위치
  relay[16],[23] : 개폐기(각각 ON, OFF)

  장치 코드 
    101~116 : 스위치(102)
    117~124 : 개폐기(112)
*/

int relay[CONNECTEDPRODURCT+COUNTRETRACT] = {2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0};
int connectCode[CONNECTEDPRODURCT ] = {102, 102, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 112, 0, 0, 0, 0, 0, 0, 0};


ModbusSerial mb;


unsigned long opTime[CONNECTEDPRODURCT] = {};
unsigned long remainTime[CONNECTEDPRODURCT] = {};

void setup() {

  /*
    relay1,2,3,4를 각각 OUTPUT으로 설정한다.
    모든 relay 상태를 HIGH(꺼짐)으로 입력한다.
  */

  for (int i = 0; i < (sizeof(relay) / sizeof(relay[0])) ; i++) {
    pinMode(relay[i], OUTPUT);
    digitalWrite(relay[i], HIGH);
  }

  /*
    Config Modbus Serial (port, speed, byte format)
  */
  mb.config(&Serial, BAUD, TXPIN);
  mb.setSlaveId(ID);


  /**
    1~8번까지 기관코드, 회사코드, 제품타입(노드:0, 양액기:1), 제품코드, 프로토콜 버전, 연결가능 디바이스수, 시리얼번호 출력
  */
  int devinfo[NODEINFO] = {0, 0, 2, 0, 10, CONNECTEDPRODURCT, 0, 0};
  for (int i = 1; i < NODEINFO + 1; ++i) {
    mb.addHreg(i, devinfo[i - 1]);
  }


  /**
    101~124 번까지 장치코드를 출력.
  */

  int l = 0 ;
  for (int i = 101; i < CONNECTEDPRODURCT + 101; ++i) {
    mb.addHreg(i, connectCode[l++]);
  }


  /**
    201번에 opid, 202번에 노드 상태를 출력.
  */
  int k = 0 ;
  for (int i = 201; i < NODESTATUS + 201; ++i) {
    mb.addHreg(i, 0);
  }


  /**
   구동기 상태를 출력
    203 : opid / 204 : 스위치 상태(0/1) / 205,206 : 남은 동작 시간 ...(반복) 출력
  */
  for (int i = READACTUATORREG ; i < 214 ; i++) {
    mb.addHreg(i, 0);
  }

  for (int i = 267 ; i < 278 ; i++) {
    mb.addHreg(i, 0);
  }

 
  /**
    501번에 opid, 502번에 노드 상태를 출력.
  */
  int m = 0 ;
  for (int i = 501; i < NODESTATUS + 501; ++i) {
    mb.addHreg(i, 0);
  }


  /**
    500번대에 제어명령을 하기 위해 모드버스 공간 확보
  */
  for (int i = WRITEACTUATORREG ; i < 514 ; i++) {
    mb.addHreg(i, 0);
  }

  for (int i = 567 ; i < 578 ; i++) {
    mb.addHreg(i, 0);
  }
}


unsigned long relayPreMillis[24] = {};

void retractinit(int a) {
  digitalWrite(relay[a], HIGH);
  digitalWrite(relay[a + COUNTRETRACT], HIGH);
}


void actuatorSwitch(int switchNum) {

  int writeSwitchReg = (switchNum * 4) + WRITEACTUATORREG; //500번대
  int readSwitchReg = (switchNum * 4) + READACTUATORREG; //200번대

  if  (mb.Hreg(writeSwitchReg + 1) != mb.Hreg(readSwitchReg)) {
    relayPreMillis[switchNum]  = millis();
    mb.Hreg(readSwitchReg, mb.Hreg(writeSwitchReg + 1));
  }
  if (mb.Hreg(writeSwitchReg) == 202) {
    opTime[switchNum] = ((unsigned long)(mb.Hreg(writeSwitchReg + 3)) << 16) + (mb.Hreg(writeSwitchReg + 2));
    mb.Hreg(readSwitchReg + 1, 201);
    digitalWrite(relay[switchNum], LOW);
    remainTime[switchNum] =  relayPreMillis[switchNum] + opTime[switchNum] * 1000 - millis();
  }
  else if (mb.Hreg(writeSwitchReg) == 0) {
    mb.Hreg(readSwitchReg + 1, 0);
    digitalWrite(relay[switchNum], HIGH);
  }

  if ((relayPreMillis[switchNum]  > 0) && (relayPreMillis[switchNum] + opTime[switchNum] * 1000 < millis())) {
    relayPreMillis[switchNum] = 0;
    mb.Hreg(writeSwitchReg, 0);
    remainTime[switchNum] = 0;
  }

  mb.Hreg(readSwitchReg + 3, int((remainTime[switchNum] >> 16) / 1000));
  mb.Hreg(readSwitchReg + 2, int(remainTime[switchNum] / 1000));

}



void actuatorRetract(int retractNum) {

  int  writeRetractReg = (retractNum * 4) + WRITEACTUATORREG; //500번대
  int readRetractReg = (retractNum * 4) + READACTUATORREG; //200번대


  if  (mb.Hreg(writeRetractReg + 1) != mb.Hreg(readRetractReg)) {
    relayPreMillis[retractNum] = millis();
    mb.Hreg(readRetractReg, mb.Hreg(writeRetractReg + 1));
  }

  if (mb.Hreg(writeRetractReg) == 303) {
    retractinit(retractNum);
    opTime[retractNum] = ((unsigned long)(mb.Hreg(writeRetractReg + 3)) << 16) + (mb.Hreg(writeRetractReg + 2));
    mb.Hreg(readRetractReg + 1, 301);
    digitalWrite(relay[retractNum], LOW);
    remainTime[retractNum] =  relayPreMillis[retractNum] + opTime[retractNum] * 1000 - millis();
  }

  else if (mb.Hreg(writeRetractReg) == 304) {
    retractinit(retractNum);
    opTime[retractNum] = ((unsigned long)(mb.Hreg(writeRetractReg + 3)) << 16) + (mb.Hreg(writeRetractReg + 2));
    mb.Hreg(readRetractReg + 1, 302);
    digitalWrite(relay[retractNum+COUNTRETRACT ], LOW);
    remainTime[retractNum] =  relayPreMillis[retractNum] + opTime[retractNum] * 1000 - millis();
  }

  else if (mb.Hreg(writeRetractReg) == 0) {
    retractinit(retractNum);
    mb.Hreg(readRetractReg+1, 0);
  }

  if ((relayPreMillis[retractNum] > 0) && (relayPreMillis[retractNum] + opTime[retractNum] * 1000 < millis())) {
    relayPreMillis[retractNum] = 0;
    mb.Hreg(writeRetractReg, 0);
    remainTime[retractNum] = 0;
  }

  mb.Hreg(readRetractReg + 3, int((remainTime[retractNum] >> 16) / 1000));
  mb.Hreg(readRetractReg + 2, int(remainTime[retractNum] / 1000));

}

void loop() {

  mb.task ();

  for(int connActuator = 0 ; connActuator < sizeof(connectCode)/sizeof(connectCode[0]) ; connActuator++ ){
    if (connectCode[connActuator] != 0 && connActuator < 16){
      actuatorSwitch(connActuator);
    }
    else if(connectCode[connActuator] != 0 && connActuator >= 16){
      actuatorRetract(connActuator);
    }
  }

}
