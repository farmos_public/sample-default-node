# KS X 3267 Reference Node based on Default Register Map 

KS X 3267 레퍼런스 센서/구동기 노드


## 소개 
본 프로젝트는 KS-X-3267 표준으로 통신이 가능한 디폴트 노드 장비를 제작하기 위한 프로젝트이다. 

현재 KS X 3267은 노드와 제어기 사이에 레지스터 맵을 공유하기위한 방법을 정의하고 있지 않은 문제점이 있다. 
이에 "스마트팜 ICT기자재 국가표준확산지원사업"에서는 디폴트로 활용할 수 있는 레지스터맵을 정의하고 있다. 

본 프로젝트에서는 디폴트 레지스터맵을 활용한 센서노드와 구동기노드를 오픈소스로 제작하여 공개한다. 

## KS X 3267 소개

KS X 3267은 "스마트 온실 센서/구동기 및 제어기간 RS485기반 MODBUS 인터페이스" 표준이다. 스마트 온실에서 센서 와 제어기 혹은 구동기와 제어기 간에 RS485기반 MODBUS 인터페이스를 사용하여 통신을 하도록 하는 표준이라고 할 수 있다. 

자세한 내용은 [국립전파연구원](https://rra.go.kr/ko/m/reference/kcsList_view.do?nb_seq=2284&cpage=2&nb_type=6&searchCon=&searchTxt=&sortOrder=) 홈페이지에서 확인이 가능하다.

## 레퍼런스 노드

본 프로젝트에서 제작된 레퍼런스 노드는 KS X 3267을 지원하는 센서 및 구동기노드를 오픈소스로 제작하여, 기관 혹은 기업에서 쉽게 구현을 확인하고, 표준을 쉽게 접목할 수 있도록 돕기위한 용도이다.

센서노드는 온/습도 센서를 활용하여, 2개의 센서가 연결된 형태로 개발되었으며, 시중에서 쉽게 구할 수 있는 DHT22 센서를 활용하였다. 구동기노드는 2개의 스위치와 1개의 개폐기를 구현하기 위해 4채널 릴레이 모듈을 사용하였다. 

센서노드 및 구동기노드에 대한 상세한 정보는 각각의 폴더에서 확인이 가능하다.

## 개발  관련  공통  사항 

* 개발환경 
  * [아두이노](https://www.arduino.cc) 

* 테스트 장비
  * [Arduino Uno](https://store.arduino.cc/usa/arduino-uno-rev3)
  * [4 채널 릴레이 모듈](https://www.devicemart.co.kr/goods/view?no=1327545)
  * [DHT22 센서](https://www.devicemart.co.kr/goods/view?no=1289976)

* 라이브러리 
  * [modbus-arduino](https://github.com/andresarmento/modbus-arduino) : Modbus 라이브러리 
  * [DHT22](https://github.com/adafruit/DHT-sensor-library/blob/master/DHT.cpp) : 온습도 센서 (DHT 라이브러리)

## 문의

* 서울대학교 농업생명과학연구원 김준용 책임연구원 tombraid at snu.ac.kr
* 주식회사 파모스 최은채 연구원 cec0909 at farmos.co.kr

## 사사
본 프로젝트는 농업기술실용화재단과 한국농기계공업협동조합의 지원으로 진행되었습니다.

 
